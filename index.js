const express = require( 'express' );
const multer = require( 'multer' );
const path = require( 'path' );
const app = express( );
const port = 3000;

const validExt = [ '.svg', '.png' ];
const upload = multer( { 
    // dest: 'uploads/', 
    fileFilter: function( req, f, callback ) {
        if( validExt.indexOf( path.extname( f.originalname ) ) == -1 ) {
            callback( null, false );
        } else {
            callback( null, true );
        }
    },
    limits: {
        fileSize: 1024 * 1024 * 5 // 5mb limit
    }
} );

// Question 1
app.get( '/hello', ( req, res ) => res.send( 'Hello World' ) );

// Question 2
app.get( '/question2', ( req, res ) => res.send( req.query.content ) );

// Question 3
app.post( '/question3', upload.array( 'photos' ), function( req, res, next ) {
    if( req.files.length == 0 ) {
        res.send( `No valid file of extension ${validExt.join(',')} specified.` );
    } else {
        res.send( `Files: ${req.files.map( f => f.originalname )} uploaded successfully.` );
    }
} );

app.listen( port, () => console.log( `App listening on port ${port}!` ) );

module.exports = app;