# Notes

## Question 3
- Files are expected to be in field `photos` with file extension `svg` or `png`. 
- An improvement could be checking mimetype of uploaded files or checking "magic numbers" of files to ensure that they are in-fact `png` or `svg` files.

## Run
```
npm install
npm test
npm start
localhost:3000/hello
localhost:3000/question2?content=hello
```