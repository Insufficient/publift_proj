const request = require( 'supertest' );
const app = require( '../index' );

describe( 'GET /hello', function( ) {
    it( 'Respond with hello world', function( done ) {
        request( app )
            .get( '/hello' )
            .expect( 200 )
            .expect( 'Hello World', done );
    } );
} );

describe( 'GET /question2', function( ) {
    it( 'Respond with nothing as no content provided', function( done ) {
        request( app )
            .get( '/question2' )
            .expect( 200 )
            .expect( '', done );
    } );

    it( 'Respond with nothing as no content provided', function( done ) {
        request( app )
            .get( '/question2?hello=1' )
            .expect( 200 )
            .expect( '', done );
    } );

    it( 'Respond with hello', function( done ) {
        request( app )
            .get( '/question2?content=hello' )
            .expect( 200 )
            .expect( 'hello', done );
    } );

    it( 'Respond with 11111', function( done ) {
        request( app )
            .get( '/question2?content=11111' )
            .expect( 200 )
            .expect( '11111', done );
    } );
} );

describe( 'POST /question3', function( ) {
    it( 'Respond with error msg (single .txt)', function( done ) {
        request( app )
            .post( '/question3' )
            .attach( 'photos', 'test/2.txt' )
            .expect( 200 )
            .expect( 'No valid file of extension .svg,.png specified.', done );
    } );

    it( 'Respond with error msg (single .md)', function( done ) {
        request( app )
            .post( '/question3' )
            .attach( 'photos', 'test/3.md' )
            .expect( 200 )
            .expect( 'No valid file of extension .svg,.png specified.', done );
    } );

    it( 'Respond successfully (single .png)', function( done ) {
        request( app )
            .post( '/question3' )
            .attach( 'photos', 'test/1.png' )
            .expect( 200 )
            .expect( 'Files: 1.png uploaded successfully.', done );
    } );

    it( 'Respond successfully (single .svg)', function( done ) {
        request( app )
            .post( '/question3' )
            .attach( 'photos', 'test/4.svg' )
            .expect( 200 )
            .expect( 'Files: 4.svg uploaded successfully.', done );
    } );

    it( 'Respond successfully (.svg + .png)', function( done ) {
        request( app )
            .post( '/question3' )
            .attach( 'photos', 'test/4.svg' )
            .attach( 'photos', 'test/1.png' )
            .expect( 200 )
            .expect( 'Files: 4.svg,1.png uploaded successfully.', done );
    } );

    it( 'Respond successfully for svg/png but not others (.svg,.png,.txt)', function( done ) {
        request( app )
            .post( '/question3' )
            .attach( 'photos', 'test/4.svg' )
            .attach( 'photos', 'test/1.png' )
            .attach( 'photos', 'test/2.txt' )
            .expect( 200 )
            .expect( 'Files: 4.svg,1.png uploaded successfully.', done );
    } );
} );